# -*- coding: utf-8 -*-

from anytree.exporter import JsonExporter
from flask import current_app, Blueprint, Response, abort, jsonify, make_response
from core.domain.chmm import Chmm
import flask_app.services.monitoring_target as monitoring_target_service

monitoring_targets_api = Blueprint('monitoring_targets', __name__, url_prefix='/api/v1/monitoring-targets')

@monitoring_targets_api.route('/', methods=['GET'], strict_slashes=False)
def list_targets():
  """List all the available monitoring targets"""
  targets = monitoring_target_service.get_targets()
  return jsonify(targets)
