# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . import monitoring_actuation_bridge_pb2 as monitoring__actuation__bridge__pb2


class MonitoringActuationBridgeStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.ListTargets = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/ListTargets',
        request_serializer=monitoring__actuation__bridge__pb2.ListTargetsRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.ListTargetsResponse.FromString,
        )
    self.CreateMonitoringSystem = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/CreateMonitoringSystem',
        request_serializer=monitoring__actuation__bridge__pb2.CreateMonitoringSystemRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.CreateMonitoringSystemResponse.FromString,
        )
    self.UpdateMonitoringSystem = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/UpdateMonitoringSystem',
        request_serializer=monitoring__actuation__bridge__pb2.UpdateMonitoringSystemRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.UpdateMonitoringSystemResponse.FromString,
        )
    self.DeleteMonitoringSystem = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/DeleteMonitoringSystem',
        request_serializer=monitoring__actuation__bridge__pb2.DeleteMonitoringSystemRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.DeleteMonitoringSystemResponse.FromString,
        )
    self.GetMonitoringSystemStatus = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/GetMonitoringSystemStatus',
        request_serializer=monitoring__actuation__bridge__pb2.GetMonitoringSystemStatusRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.GetMonitoringSystemStatusResponse.FromString,
        )
    self.ReconfigureDataEngine = channel.unary_unary(
        '/varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge/ReconfigureDataEngine',
        request_serializer=monitoring__actuation__bridge__pb2.ReconfigureDataEngineRequest.SerializeToString,
        response_deserializer=monitoring__actuation__bridge__pb2.ReconfigureDataEngineResponse.FromString,
        )


class MonitoringActuationBridgeServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def ListTargets(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateMonitoringSystem(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def UpdateMonitoringSystem(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def DeleteMonitoringSystem(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetMonitoringSystemStatus(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ReconfigureDataEngine(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_MonitoringActuationBridgeServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'ListTargets': grpc.unary_unary_rpc_method_handler(
          servicer.ListTargets,
          request_deserializer=monitoring__actuation__bridge__pb2.ListTargetsRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.ListTargetsResponse.SerializeToString,
      ),
      'CreateMonitoringSystem': grpc.unary_unary_rpc_method_handler(
          servicer.CreateMonitoringSystem,
          request_deserializer=monitoring__actuation__bridge__pb2.CreateMonitoringSystemRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.CreateMonitoringSystemResponse.SerializeToString,
      ),
      'UpdateMonitoringSystem': grpc.unary_unary_rpc_method_handler(
          servicer.UpdateMonitoringSystem,
          request_deserializer=monitoring__actuation__bridge__pb2.UpdateMonitoringSystemRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.UpdateMonitoringSystemResponse.SerializeToString,
      ),
      'DeleteMonitoringSystem': grpc.unary_unary_rpc_method_handler(
          servicer.DeleteMonitoringSystem,
          request_deserializer=monitoring__actuation__bridge__pb2.DeleteMonitoringSystemRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.DeleteMonitoringSystemResponse.SerializeToString,
      ),
      'GetMonitoringSystemStatus': grpc.unary_unary_rpc_method_handler(
          servicer.GetMonitoringSystemStatus,
          request_deserializer=monitoring__actuation__bridge__pb2.GetMonitoringSystemStatusRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.GetMonitoringSystemStatusResponse.SerializeToString,
      ),
      'ReconfigureDataEngine': grpc.unary_unary_rpc_method_handler(
          servicer.ReconfigureDataEngine,
          request_deserializer=monitoring__actuation__bridge__pb2.ReconfigureDataEngineRequest.FromString,
          response_serializer=monitoring__actuation__bridge__pb2.ReconfigureDataEngineResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'varys.monitoring_actuation_bridge.v1.MonitoringActuationBridge', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
