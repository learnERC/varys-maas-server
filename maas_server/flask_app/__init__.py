# -*- coding: utf-8 -*-

from flask import Flask
from healthcheck import HealthCheck, EnvironmentDump
from flask_cors import CORS
from flask_app.utils.redis import rq
from flask_app.utils.json import CustomJSONEncoder
from flask_app.blueprints import chmm, monitoring_request, monitoring_target
import flask_app.services.chmm as chmm_service
import flask_app.services.monitoring_system as monitoring_system_service
import flask_app.services.data_engine as data_engine_service
from core.repositories.utils import mongodb_connect

def create_app(test_config=None):
  app = Flask(__name__)
  app.config.from_object(__name__ + '.settings')
  if test_config is not None:
      app.config.update(test_config)
  app.json_encoder = CustomJSONEncoder

  HealthCheck(app, '/_meta/status')
  if app.env != 'production':
    EnvironmentDump(app, '/_meta/environment')

  chmm_service.init_app(app)

  mongodb_connect(app.config['MONGO_URI'], app.config['APP_NAME'])

  rq.init_app(app)
  rq.default_queue = app.config['RQ_DEFAULT_QUEUE']
  # Schedule a job every 5 minutes to check for deployed monitoring requests
  monitoring_system_service.check_monitoring_requests_status.cron('*/2 * * * *', 'check-deployed-requests', 'Deployed')
  # Schedule a job every 1 minute to check for deploying monitoring requests
  monitoring_system_service.check_monitoring_requests_status.cron('* * * * *', 'check-deploying-requests', 'Deploying')
  # Schedule a job every 1 minute to enqueue monitoring requests which need a redeployment
  monitoring_system_service.enqueue_redeployment_jobs.cron('* * * * *', 'enqueue-redeployment-requests')
  if app.config['DATA_ENGINE_NEEDS_RECONF']:
    # Schedule a job every 1 minute to enqueue reconfigure jobs
    data_engine_service.enqueue_reconfigure_jobs.cron('* * * * *', 'enqueue-reconfigure-jobs')

  CORS(app)
  app.register_blueprint(chmm.chmm_api)
  app.register_blueprint(monitoring_request.monitoring_requests_api)
  app.register_blueprint(monitoring_target.monitoring_targets_api)

  return app
