# -*- coding: utf-8 -*-

from bson import ObjectId
from flask.json import JSONEncoder
from core.domain.chmm import Goal
from core.domain.probe import Probe
from core.domain.monitoring import MonitoringRequest, Target

class CustomJSONEncoder(JSONEncoder):
  def default(self, obj):
    if isinstance(obj, ObjectId):
      return str(obj)
    if isinstance(obj, Target):
      return {
        'id': obj.id,
        'name': obj.name,
        'source': obj.source,
        'environment': obj.environment,
        'metadata': obj.metadata,
      }
    if isinstance(obj, MonitoringRequest):
      return {
        '_id': obj._id,
        'target': obj.target,
        'selected_goals': obj.selected_goals,
        'current_goals': obj.current_goals,
        'selected_probes': obj.selected_probes,
        'current_probes': obj.current_probes,
        'monitoring_units': obj.monitoring_units,
        'pattern': obj.pattern,
        'status': obj.status,
        'created_at': obj.created_at,
        'updated_at': obj.updated_at,
      }
    if isinstance(obj, Goal):
      return {
        'name': obj.name,
      }
    if isinstance(obj, Probe):
      return {
        '_id': obj._id,
        'name': obj.name,
        'human_readable_name': obj.human_readable_name,
        'supported_cloud_properties': obj.cloud_properties,
        'supported_target_envs': obj.supported_target_envs,
        'supported_data_engines': obj.supported_data_engines,
        'configurable_parameters': obj.configurable_parameters,
      }
    return super(CustomJSONEncoder, self).default(obj)
