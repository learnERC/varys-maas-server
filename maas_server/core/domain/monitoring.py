# -*- coding: utf-8 -*-

from pymodm import MongoModel, EmbeddedMongoModel, EmbeddedDocumentField, fields
from pymodm.manager import Manager
from pymodm.queryset import QuerySet
from core.monitoring_actuation_bridge_pb2 import Environment, Pattern
import os

class Target(EmbeddedMongoModel):
  """The class which represents a monitoring target."""
  id = fields.CharField(required=True, blank=True)
  name = fields.CharField(required=True, blank=True)
  source = fields.CharField(required=True, blank=True)
  environment = fields.CharField(choices=Environment.DESCRIPTOR.values_by_name.keys(), required=True, blank=True)
  metadata = fields.DictField(required=True, blank=True)

  class Meta:
    connection_alias = os.environ.get('APP_NAME')

  def __repr__(self):
    return '{}(id={!r}, name={!r}, source={!r}, environment={!r}, metadata={!r})'.format(
      self.__class__.__name__,
      self.id,
      self.name,
      self.source,
      self.environment,
      self.metadata,
    )

class MonitoringRequest(MongoModel):
  """The class which represents a monitoring request."""
  selected_goals = fields.ListField(required=True)
  current_goals = fields.ListField(required=True)
  target = fields.EmbeddedDocumentField(Target, required=True)
  current_probes = fields.ListField(required=False, blank=True)
  selected_probes = fields.ListField(required=False, blank=True)
  pattern = fields.CharField(choices=Pattern.DESCRIPTOR.values_by_name.keys(), required=False, blank=True)
  status = fields.CharField(required=True, default='Created')
  monitoring_units = fields.ListField(required=False, blank=True)
  created_at = fields.DateTimeField(required=True)
  updated_at = fields.DateTimeField(required=False)

  class Meta:
    connection_alias = os.environ.get('APP_NAME')
    collection_name = 'monitoring_requests'

  def __repr__(self):
    return '{}(_id={!r}, selected_goals={!r}, current_goals={!r}, target={!r}, current_probes={!r}, selected_probes={!r}, pattern={!r}, status={!r}, monitoring_units={!r}, created_at={!r}, updated_at={!r})'.format(
      self.__class__.__name__,
      self._id,
      self.selected_goals,
      self.current_goals,
      self.target,
      self.current_probes,
      self.selected_probes,
      self.pattern,
      self.status,
      self.monitoring_units,
      self.created_at,
      self.updated_at
    )
