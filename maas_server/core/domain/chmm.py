# -*- coding: utf-8 -*-

from ruamel.yaml import YAML
from anytree import Node, NodeMixin

class Chmm(object):
  """The class which represents an implementation of CHMM"""
  def __init__(self, tree=None):
    self._tree = tree

  @classmethod
  def from_filepath(self, chmm_filepath):
    """Instantiate a CHMM from a YAML file"""
    yaml = YAML(typ='safe')
    chmm_file = open(chmm_filepath, 'r')
    chmm = yaml.load(chmm_file)

    root = Node('monitoring_goals')

    for monitoring_goal in chmm['monitoring_goals']:
      goal = MonitoringGoal(monitoring_goal['name'], parent=root)
      for monitoring_subgoal in monitoring_goal['monitoring_subgoals']:
        subgoal = MonitoringSubgoal(monitoring_subgoal['name'], parent=goal)
        for cloud_property in monitoring_subgoal['cloud_properties']:
          CloudProperty(cloud_property, parent=subgoal)
    chmm = self(root)
    return chmm

  @property
  def tree(self):
    return self._tree

  @tree.setter
  def tree(self, value):
    self._tree = value

  @tree.deleter
  def tree(self):
    del self._tree

class Goal(NodeMixin):
  """The class which represents a generic CHMM goal"""
  def __init__(self, name, parent=None):
    super(Goal, self).__init__()
    self.name = name
    self.parent = parent

  def __repr__(self):
    return '{}(name={!r})'.format(self.__class__.__name__, self.name)

class MonitoringGoal(Goal):
  """The class which represents a MonitoringGoal"""
  def __init__(self, name, parent=None):
    super(MonitoringGoal, self).__init__(name, parent)

class MonitoringSubgoal(Goal):
  """The class which represents a MonitoringSubgoal"""
  def __init__(self, name, parent=None):
    super(MonitoringSubgoal, self).__init__(name, parent)

class CloudProperty(Goal):
  """The class which represents a CloudProperty"""
  def __init__(self, name, parent=None):
    super(CloudProperty, self).__init__(name, parent)
