# -*- coding: utf-8 -*-

from anytree.exporter import JsonExporter
from flask import current_app, Blueprint, Response, abort, jsonify, make_response
from core.domain.chmm import Chmm
import flask_app.services.chmm as chmm_service

chmm_api = Blueprint('chmm', __name__, url_prefix='/api/v1/chmm')
exporter = JsonExporter(sort_keys=False)

@chmm_api.route('/', methods=['GET'], strict_slashes=False)
def show_tree():
  """Get a JSON representation of the CHMM tree"""
  chmm = chmm_service.get_chmm()
  if chmm is not None:
    return Response(exporter.export(chmm.tree), content_type='application/json; charset=utf-8')
  else:
    return abort(make_response(jsonify(error='There is no CHMM available'), 500))
