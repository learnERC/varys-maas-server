# -*- coding: utf-8 -*-

from flask import current_app
from core.domain.chmm import Chmm, CloudProperty
from anytree.search import find_by_attr

def expand_goals(goals, tree):
  """Expand to tree leaves given a list of goals and a CHMM tree"""
  leaves = list()
  for goal in goals:
    node = find_by_attr(tree, goal)
    if not node.is_leaf:
      leaves.extend([n for n in node.descendants if isinstance(n, CloudProperty)])
    else:
      leaves.append(node)
  return leaves

def get_chmm():
  """Get the CHMM object"""
  if current_app.config['CHMM_OBJ'] is None:
    self.init_chmm(current_app)
  return current_app.config['CHMM_OBJ']

def init_chmm(app=current_app):
    """Set the global CHMM object."""
    if app.config['CHMM_OBJ'] is None:
      chmm_filepath = app.static_folder + '/chmm.yml'
      app.config['CHMM_OBJ'] = Chmm.from_filepath(chmm_filepath)
    else:
      app.logger.warning('A global CHMM is already initialized.')

def init_app(app):
    """Register CHMM functions with the Flask app. This is called by
    the application factory."""
    init_chmm(app)
