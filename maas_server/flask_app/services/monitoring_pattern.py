# -*- coding: utf-8 -*-

from core import monitoring_actuation_bridge_pb2 as protobuf_messages

def detect_pattern(environment):
  patterns = {
    protobuf_messages.Environment.Name(protobuf_messages.CONTAINER): protobuf_messages.Pattern.Name(protobuf_messages.RESERVED_SIDECAR_SINGLE_PROBE),
    protobuf_messages.Environment.Name(protobuf_messages.ACCESSIBLE_VM): protobuf_messages.Pattern.Name(protobuf_messages.INTERNAL_PROBES),
    protobuf_messages.Environment.Name(protobuf_messages.INACCESSIBLE_VM): protobuf_messages.Pattern.Name(protobuf_messages.RESERVED_SIDECAR_MULTI_PROBES),
  }
  return patterns[environment]
