#!/bin/sh
set -e

envsubst < /etc/nginx/conf.d/maas_server.template > /etc/nginx/conf.d/maas_server.conf

exec nginx -g 'daemon off;'
