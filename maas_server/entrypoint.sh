#!/bin/sh
set -e

mongodb-migrate --url "$MONGO_URI" --migrations flask_app/migrations

exec "$@"
