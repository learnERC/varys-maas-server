from flask_app import create_app
import logging
import os

app = create_app()

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger(os.getenv('APP_NAME', 'maas_server'))
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
