# -*- coding: utf-8 -*-

from pymodm import MongoModel, fields
import os

class Probe(MongoModel):
  """The class which represents a probe."""
  human_readable_name = fields.CharField(required=True)
  name = fields.CharField(required=True)
  supported_cloud_properties = fields.ListField(required=True, blank=True)
  supported_target_envs = fields.ListField(required=True, blank=True)
  supported_data_engines = fields.ListField(required=True, blank=True)
  configurable_parameters = fields.ListField(required=True, blank=True)

  class Meta:
    connection_alias = os.environ.get('APP_NAME')
    collection_name = 'probes'

  def __repr__(self):
    return '{}(_id={!r}, human_readable_name={!r}, name={!r}, supported_cloud_properties={!r}, supported_target_envs={!r}, supported_data_engines={!r}, configurable_parameters={!r})'.format(
      self.__class__.__name__,
      self._id,
      self.human_readable_name,
      self.name,
      self.supported_cloud_properties,
      self.supported_target_envs,
      self.supported_data_engines,
      self.configurable_parameters
    )
