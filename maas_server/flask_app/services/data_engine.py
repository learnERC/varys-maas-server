# -*- coding: utf-8 -*-

from flask import current_app
from flask_app.utils.redis import rq
import core.repositories.monitoring as monitoring_repository
import grpc
from google.protobuf import json_format
from core import monitoring_actuation_bridge_pb2 as protobuf_messages
from core import monitoring_actuation_bridge_pb2_grpc as protobuf_grpc

@rq.job
def enqueue_reconfigure_jobs():
  '''
  This job runs only when the DATA_ENGINE_NEEDS_RECONF environment var
  is True.
  The job enqueues a reconfigure job for each of the requests in a Deployed state
  sorted by updated_at timestamp.
  '''
  monitoring_requests = list(
    monitoring_repository.on_status('Needs reconfiguration').order_by([('updated_at', -1)])
  )
  queue = rq.get_queue(current_app.config['RQ_DATA_ENGINE_RECONF_QUEUE'])
  for monitoring_request in monitoring_requests:
    queue.enqueue(
      reconfigure,
      job_timeout='5m',
      job_id=str(monitoring_request._id) + '-reconfigure-data-engine',
      args=(monitoring_request,)
    )

@rq.job('reconfigure_data_engine', timeout=60*5)
def reconfigure(monitoring_request):
  target = monitoring_request.target
  kwargs = {
    'monitoring_request_id': str(monitoring_request._id),
    'data_engine': {
      'name': current_app.config['DATA_ENGINE_NAME'],
      'credentials': current_app.config['DATA_ENGINE_CREDENTIALS'],
      'hosts': current_app.config['DATA_ENGINE_HOSTS'],
      'source': current_app.config['DATA_ENGINE_SOURCE'],
      'config_location': current_app.config['DATA_ENGINE_CONFIG_LOCATION'],
    }
  }
  reconfigure_request = protobuf_messages.ReconfigureDataEngineRequest(**kwargs)

  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    reconfigure_future = stub.ReconfigureDataEngine.future(reconfigure_request)
    reconfigure_result = reconfigure_future.result()
    # FIXME: Result?
    monitoring_request.status = 'Deployed'
    monitoring_repository.save(monitoring_request)
