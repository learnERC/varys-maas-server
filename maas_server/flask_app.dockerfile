FROM python:3-onbuild

RUN chmod +x run_tests.sh
RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
CMD ["/usr/local/bin/gunicorn", "-w2", "-b:8000", "--log-level=warn", "run:app"]
