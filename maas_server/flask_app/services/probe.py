# -*- coding: utf-8 -*-

import core.repositories.probe as probe_repository
from core.domain.probe import Probe
from flask import current_app

def extract_probes(target, cloud_properties):
  """Extracts the needed probes for a given monitoring target and the cloud properties.

  Args:
      target (Target): the monitoring target.
      cloud_properties (List[str]): the list of cloud propertie names.

  Returns:
      list: a list containing the extracted probes

  """
  probes = []

  for cloud_property in cloud_properties:
    probe = probe_repository.find_by_cloud_property_and_target_env_and_data_engine(
      cloud_property,
      target.environment,
      current_app.config['DATA_ENGINE_NAME']
    )
    probes.append({
      '_id': str(probe._id),
      'name': probe.name,
      'cloud_properties_to_monitor': [cloud_property], # FIXME: It could be expanded to enable enhanced probe setup
      'configurable_parameters': probe.configurable_parameters
    })
  return probes

def calculate_probe_sets(current_probes, selected_probes):
  to_remove_ids = set([c['_id'] for c in current_probes]).difference(set([c['_id'] for c in selected_probes]))
  to_add_ids = set([c['_id'] for c in selected_probes]).difference(set([c['_id'] for c in current_probes]))

  to_add_probes = [p for p in selected_probes if p['_id'] in to_add_ids]
  to_remove_probes = [p for p in current_probes if p['_id'] in to_remove_ids]

  return to_add_probes, to_remove_probes
