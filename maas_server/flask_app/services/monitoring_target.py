# -*- coding: utf-8 -*-

import grpc
from google.protobuf import json_format
from flask import current_app
from core import monitoring_actuation_bridge_pb2 as protobuf_messages
from core import monitoring_actuation_bridge_pb2_grpc as protobuf_grpc

def get_targets():
  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    list_targets_future = stub.ListTargets.future(
      protobuf_messages.ListTargetsRequest()
    )
    list_targets_result = list_targets_future.result()
    list_targets_result_dict = json_format.MessageToDict(list_targets_result, preserving_proto_field_name = True)
    return list_targets_result_dict['targets'] if list_targets_result_dict.get('targets') else []
