# VARYS MaaS Server

## Synopsis
This is a Proof of Concept (POC) for the VARYS MaaS Server.
The main aim is to provide a microservice to manage dynamic monitoring requests configuring and deploying the probes.

The application is built with [Flask](http://flask.pocoo.org/), a Python microframework.

## How to execute
In order to create a reproducible environment a Dockerfiles are available.

Also, a `docker-compose.*.yml` files are provided to speed-up development and lay the foundations for a more complex setup.

> N.B.: all the commands are intended to be executed from the project root directory (`maas-server/`)

### Docker Compose execution
Requirements to run with Docker Compose:

* Docker Engine 18.02.0+
* Docker Compose 1.20.0+

Run the following command to build the services:
```
$ docker-compose build
```

Then, run the following command to run the services:
```
$ docker-compose up
```

## API
> N.B.: the API is UNSTABLE

All the API endpoints have the `/api/v1` prefix.

The currently available endpoints are:

* `/chmm`:
  - `GET`: returns a JSON representation of the CHMM loaded tree
* `/monitoring-targets`:
  - `GET`: returns a JSON array with all the currently available monitoring targets
* `/monitoring-requests`:
  - `GET`: returns a JSON array with all the currently available monitoring requests
  - `POST`: returns a JSON object with the taken over monitoring request or an error
  Accepts a JSON representation of the [monitoring_definition.example.yml](maas_server/monitoring_definition.example.yml) file as payload.
* `/monitoring-requests/{monitoring_id}`:
  - `PATCH|PUT`: returns a JSON object with the taken over monitoring request or an error
  Accepts a JSON representation of the [monitoring_definition.example.yml](maas_server/monitoring_definition.example.yml) file as payload
  - `DELETE`: returns a JSON object with the status of the request or an error


## How to run tests
A dedicated Docker Compose file (`docker-compose.tests.yml`) is available to run tests.

Run the following commmand to execute the tests:
```
$ docker-compose -f docker-compose.tests.yml up
```

You if are not using Docker Compose you can manually override the Docker entrypoint to execute the tests:
```
$ docker run -it \
  --name maas-server-tests \
  --entrypoint "./run_tests.sh" \
  --env-file .env
  ngpaas/maas-server:latest
```

## License
This project is licensed under the AGPLv3. See the [LICENSE.md](LICENSE.md) file for details.
