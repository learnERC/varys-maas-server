# -*- coding: utf-8 -*-

from core.domain.probe import Probe
from bson.objectid import ObjectId

def count():
  return Probe.objects.all().count()

def find_all():
  return Probe.objects.all()

def find(id):
  return Probe.objects.get({'_id': ObjectId(id)})

def delete(id):
  return Probe.objects.get({'_id': ObjectId(id)}).delete()

def save(probe):
  return probe.save()

def find_by_cloud_property_and_target_env_and_data_engine(cloud_property, target_environment, data_engine):
  return Probe.objects.get({
    'supported_cloud_properties': cloud_property,
    'supported_target_envs': target_environment,
    'supported_data_engines': data_engine
  })
