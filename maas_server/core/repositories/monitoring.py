# -*- coding: utf-8 -*-

from core.domain.monitoring import MonitoringRequest
from bson.objectid import ObjectId

def count():
  counter = MonitoringRequest.objects.all().count()
  return counter

def find_all():
  return MonitoringRequest.objects.all()

def find(id):
  return MonitoringRequest.objects.get({'_id': ObjectId(id)})

def delete(id):
  return MonitoringRequest.objects.get({'_id': ObjectId(id)}).delete()

def save(monitoring_request):
  return monitoring_request.save()

def on_status(status):
  return MonitoringRequest.objects.raw({'status': status})
