# -*- coding: utf-8 -*-

from mongodb_migrations.base import BaseMigration

class Migration(BaseMigration):
  def upgrade(self):
    self.db.probes.save({
      'human_readable_name': 'Metricbeat CPU Module',
      'name': 'metricbeat_cpu_consumption',
      'supported_target_envs': ['CONTAINER', 'ACCESSIBLE_VM'],
      'supported_cloud_properties': ['cpu_consumption'],
      'supported_data_engines': ['kafka', 'logstash', 'elasticsearch'],
      'configurable_parameters': ['period', 'metricset_maps'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'Metricbeat Memory Module',
      'name': 'metricbeat_memory_consumption',
      'supported_target_envs': ['CONTAINER', 'ACCESSIBLE_VM'],
      'supported_cloud_properties': ['memory_consumption'],
      'supported_data_engines': ['kafka', 'logstash', 'elasticsearch'],
      'configurable_parameters': ['period', 'metricset_maps'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'Metricbeat MongoDB Module',
      'name': 'metricbeat_mongodb',
      'supported_target_envs': ['CONTAINER', 'INACCESSIBLE_VM', 'ACCESSIBLE_VM'],
      'supported_cloud_properties': ['status', 'uptime'],
      'supported_data_engines': ['kafka', 'logstash', 'elasticsearch'],
      'configurable_parameters': ['period', 'metricset_maps'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'kube-state-metrics exporter container restarts',
      'name': 'kube_state_metrics_exporter',
      'supported_target_envs': ['CONTAINER'],
      'supported_cloud_properties': ['number_of_failures'],
      'supported_data_engines': ['prometheus'],
      'configurable_parameters': ['scrape_interval'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'cAdvisor CPU metrics exporter',
      'name': 'cadvisor_cpu_exporter',
      'supported_target_envs': ['CONTAINER'],
      'supported_cloud_properties': ['cpu_consumption'],
      'supported_data_engines': ['prometheus'],
      'configurable_parameters': ['scrape_interval'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'cAdvisor memory metrics exporter',
      'name': 'cadvisor_memory_exporter',
      'supported_target_envs': ['CONTAINER'],
      'supported_cloud_properties': ['memory_consumption'],
      'supported_data_engines': ['prometheus'],
      'configurable_parameters': ['scrape_interval'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'cAdvisor network inbound metrics exporter',
      'name': 'cadvisor_network_inbound_exporter',
      'supported_target_envs': ['CONTAINER'],
      'supported_cloud_properties': ['inbound'],
      'supported_data_engines': ['prometheus'],
      'configurable_parameters': ['scrape_interval'],
      '_cls' : 'core.domain.probe.Probe'
    })
    self.db.probes.save({
      'human_readable_name': 'cAdvisor network outbound metrics exporter',
      'name': 'cadvisor_network_outbound_exporter',
      'supported_target_envs': ['CONTAINER'],
      'supported_cloud_properties': ['outbound'],
      'supported_data_engines': ['prometheus'],
      'configurable_parameters': ['scrape_interval'],
      '_cls' : 'core.domain.probe.Probe'
    })

  def downgrade(self):
    self.db.probes.drop()
