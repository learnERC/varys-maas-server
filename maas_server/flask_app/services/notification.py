# -*- coding: utf-8 -*-

from flask import current_app
from flask_app.utils.redis import rq
from confluent_kafka import Producer

def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        current_app.logger.error('Message delivery failed: {}'.format(err))
    else:
        current_app.logger.info('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

@rq.job
def notify_shipped_monitoring(monitoring_request_id, action):

  conf = {
    'bootstrap.servers': ','.join(current_app.config['KAFKA_BROKERS'])
  }

  producer = Producer(**conf)

  # Trigger any available delivery report callbacks from previous produce() calls
  producer.poll(0)

  data = {
    'monitoring_request_id': str(monitoring_request_id),
    'template_id': 1,
  }

  # Asynchronously produce a message, the delivery report callback
  # will be triggered from poll() above, or flush() below, when the message has
  # been successfully delivered or failed permanently.
  producer.produce(current_app.config['KAFKA_TOPIC'], str(data).encode('utf-8'), callback=delivery_report)

  # Wait for any outstanding messages to be delivered and delivery report
  # callbacks to be triggered.
  producer.flush()
