# -*- coding: utf-8 -*-

import os, json
import grpc
from google.protobuf import json_format
from flask import current_app
from core.domain.monitoring import MonitoringRequest, Target
import core.repositories.monitoring as monitoring_repository
import flask_app.services.chmm as chmm_service
import flask_app.services.probe as probe_service
import flask_app.services.monitoring_pattern as monitoring_pattern_service
import flask_app.services.notification as notification_service
from flask_app.utils.redis import rq
from datetime import datetime, timedelta
from core import monitoring_actuation_bridge_pb2 as protobuf_messages
from core import monitoring_actuation_bridge_pb2_grpc as protobuf_grpc

def get_all():
  return monitoring_repository.find_all()

def get(monitoring_request_id):
  return monitoring_repository.find(monitoring_request_id)

def create(target, selected_goals):
  created_at = datetime.now()
  updated_at = created_at
  status = 'Created'
  monitoring_request = MonitoringRequest(
    target=Target(**target),
    selected_goals=selected_goals,
    current_goals=selected_goals,
    status=status,
    created_at=created_at,
    updated_at=updated_at
  )
  monitoring_request = monitoring_repository.save(monitoring_request)

  _create_job_chain(monitoring_request, 'create')
  return monitoring_request

def update(monitoring_request_id, goals):
  monitoring_request = monitoring_repository.find(monitoring_request_id)
  monitoring_request.selected_goals = goals
  monitoring_request.updated_at = datetime.now()
  monitoring_request.status = 'Updated'
  monitoring_repository.save(monitoring_request)

  _create_job_chain(monitoring_request, 'update')
  return monitoring_request

def delete(monitoring_request_id):
  monitoring_request = monitoring_repository.find(monitoring_request_id)
  target = monitoring_request.target
  kwargs = {
    'monitoring_request_id': str(monitoring_request._id),
    'target': {
      'id': target.id,
      'source': target.source,
      'metadata': target.metadata,
    },
    'pattern': protobuf_messages.Pattern.Value(monitoring_request.pattern),
    'probes': [
      {
        'id': probe['_id'],
        'name': probe['name'],
        'cloud_properties_to_monitor': probe['cloud_properties_to_monitor'],
        'configurable_parameters': probe['configurable_parameters']
      } for probe in monitoring_request.current_probes
    ],
    'data_engine': {
      'name': current_app.config['DATA_ENGINE_NAME'],
      'hosts': current_app.config['DATA_ENGINE_HOSTS'],
      'credentials': current_app.config['DATA_ENGINE_CREDENTIALS'],
      'source': current_app.config['DATA_ENGINE_SOURCE'],
      'config_location': current_app.config['DATA_ENGINE_CONFIG_LOCATION'],
      'needs_reconfiguration': current_app.config['DATA_ENGINE_NEEDS_RECONF']
    },
  }
  if monitoring_request.monitoring_units is not None:
    kwargs['monitoring_units'] = [
      {
        'id': mu['id'],
        'name': mu['name'],
        'probe_ids': mu['probe_ids'],
      } for mu in monitoring_request.monitoring_units
    ]

  delete_monitoring_system_request = protobuf_messages.DeleteMonitoringSystemRequest(**kwargs)

  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    delete_monitoring_future = stub.DeleteMonitoringSystem.future(delete_monitoring_system_request)
    delete_monitoring_result = delete_monitoring_future.result()
    return monitoring_repository.delete(monitoring_request_id)

def _create_job_chain(monitoring_request, action):
  queue = rq.get_queue()

  prepare_monitoring_job = queue.enqueue(
    prepare_monitoring,
    job_id=str(monitoring_request._id) + '-prepare_monitoring',
    args=(str(monitoring_request._id),)
  )

  ship_monitoring_job = queue.enqueue(
    ship_monitoring,
    job_timeout='5m',
    job_id=str(monitoring_request._id) + '-ship_monitoring',
    args=(str(monitoring_request._id), action,),
    depends_on=prepare_monitoring_job
  )

  # notify_shipped_monitoring_job = queue.enqueue(
  #   notification_service.notify_shipped_monitoring,
  #   job_id=str(monitoring_request._id) + '-notify_shipped_monitoring',
  #   args=(str(monitoring_request._id), action,),
  #   depends_on=ship_monitoring_job
  # )

def _ship_create(monitoring_request):
  target = monitoring_request.target
  kwargs = {
    'monitoring_request_id': str(monitoring_request._id),
    'target': {
      'id': target.id,
      'source': target.source,
      'metadata': target.metadata,
    },
    'pattern': protobuf_messages.Pattern.Value(monitoring_request.pattern),
    'probes': [
      {
        'id': probe['_id'],
        'name': probe['name'],
        'cloud_properties_to_monitor': probe['cloud_properties_to_monitor'],
        'configurable_parameters': probe['configurable_parameters']
      } for probe in monitoring_request.selected_probes
    ],
    'data_engine': {
      'name': current_app.config['DATA_ENGINE_NAME'],
      'hosts': current_app.config['DATA_ENGINE_HOSTS'],
      'credentials': current_app.config['DATA_ENGINE_CREDENTIALS'],
      'source': current_app.config['DATA_ENGINE_SOURCE'],
      'config_location': current_app.config['DATA_ENGINE_CONFIG_LOCATION'],
      'needs_reconfiguration': current_app.config['DATA_ENGINE_NEEDS_RECONF']
    },
  }

  create_monitoring_system_request = protobuf_messages.CreateMonitoringSystemRequest(**kwargs)

  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    create_monitoring_system_future = stub.CreateMonitoringSystem.future(create_monitoring_system_request)
    create_monitoring_system_result = create_monitoring_system_future.result()
    if create_monitoring_system_result.monitoring_units:
      result_message = json_format.MessageToDict(create_monitoring_system_result, preserving_proto_field_name = True)
      monitoring_request.monitoring_units = result_message['monitoring_units']
      monitoring_repository.save(monitoring_request)

def _ship_update(monitoring_request):
  probes_to_add, probes_to_remove = probe_service.calculate_probe_sets(
    monitoring_request.current_probes,
    monitoring_request.selected_probes
  )
  target = monitoring_request.target
  kwargs = {
    'monitoring_request_id': str(monitoring_request._id),
    'target': {
      'id': target.id,
      'source': target.source,
      'metadata': target.metadata,
    },
    'pattern': protobuf_messages.Pattern.Value(monitoring_request.pattern),
    'probes_to_add': [
      {
        'id': probe['_id'],
        'name': probe['name'],
        'cloud_properties_to_monitor': probe['cloud_properties_to_monitor'],
        'configurable_parameters': probe['configurable_parameters']
      } for probe in probes_to_add
    ],
    'probes_to_remove': [
      {
        'id': probe['_id'],
        'name': probe['name'],
        'cloud_properties_to_monitor': probe['cloud_properties_to_monitor'],
        'configurable_parameters': probe['configurable_parameters']
      } for probe in probes_to_remove
    ],
    'monitoring_units': [
      {
        'id': mu['id'],
        'name': mu['name'],
        'probe_ids': mu['probe_ids'],
      } for mu in monitoring_request.monitoring_units
    ],
    'data_engine': {
      'name': current_app.config['DATA_ENGINE_NAME'],
      'hosts': current_app.config['DATA_ENGINE_HOSTS'],
      'credentials': current_app.config['DATA_ENGINE_CREDENTIALS'],
      'source': current_app.config['DATA_ENGINE_SOURCE'],
      'config_location': current_app.config['DATA_ENGINE_CONFIG_LOCATION'],
      'needs_reconfiguration': current_app.config['DATA_ENGINE_NEEDS_RECONF']
    },
  }

  update_monitoring_system_request = protobuf_messages.UpdateMonitoringSystemRequest(**kwargs)

  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    update_monitoring_system_future = stub.UpdateMonitoringSystem.future(update_monitoring_system_request)
    update_monitoring_system_result = update_monitoring_system_future.result()

    if update_monitoring_system_result.monitoring_units:
      result_message = json_format.MessageToDict(update_monitoring_system_result, preserving_proto_field_name = True)
      monitoring_request.monitoring_units = result_message['monitoring_units']
      monitoring_repository.save(monitoring_request)

@rq.job
def prepare_monitoring(monitoring_request_id):
  monitoring_request = monitoring_repository.find(monitoring_request_id)
  target = monitoring_request.target
  goals = monitoring_request.selected_goals

  monitoring_request.status = 'Building'
  monitoring_repository.save(monitoring_request)

  try:
    monitoring_request.pattern = monitoring_pattern_service.detect_pattern(
      monitoring_request.target.environment
    )
  except KeyError as e:
    raise RuntimeError('Unsupported target environment. No pattern identified.')

  current_app.logger.info("Expanding goals for %s...", target.id)
  cloud_properties = chmm_service.expand_goals(goals, current_app.config['CHMM_OBJ'].tree)

  current_app.logger.info("Mapping probes for %s...", target.id)
  monitoring_request.selected_probes = probe_service.extract_probes(target, [c.name for c in cloud_properties])

  monitoring_repository.save(monitoring_request)

@rq.job(timeout=60*5)
def ship_monitoring(monitoring_request_id, action):
  monitoring_request = monitoring_repository.find(monitoring_request_id)
  monitoring_request.status = 'Deploying'
  monitoring_repository.save(monitoring_request)
  current_app.logger.info("Delegating deployment for %s...", monitoring_request.target.id)

  if action == 'create':
    _ship_create(monitoring_request)
  elif action == 'update':
    _ship_update(monitoring_request)

@rq.job
def enqueue_redeployment_jobs():
  '''
  This job fires a new create job chain for each of the requests in a Needs redeployment state.
  '''
  monitoring_requests = list(
    monitoring_repository.on_status('Needs redeployment')
  )
  for monitoring_request in monitoring_requests:
    _create_job_chain(monitoring_request, 'create')

@rq.job
def check_monitoring_requests_status(status):
  '''
  Get all the monitoring requests with the specified status and
  enqueue a dedicated job to check and update (eventually) the deployment status.
  '''
  monitoring_requests = list(monitoring_repository.on_status(status))
  queue = rq.get_queue(current_app.config['RQ_STATUSES_SYNC_QUEUE'])
  for monitoring_request in monitoring_requests:
    queue.enqueue(synch_monitoring_request_status, args=(monitoring_request,))

@rq.job
def synch_monitoring_request_status(monitoring_request):
  target = monitoring_request.target
  kwargs = {
    'monitoring_request_id': str(monitoring_request._id),
    'target': {
      'id': target.id,
      'source': target.source,
      'metadata': target.metadata,
    },
    'pattern': protobuf_messages.Pattern.Value(monitoring_request.pattern),
    'probes': [
      {
        'id': probe['_id'],
        'name': probe['name'],
        'cloud_properties_to_monitor': probe['cloud_properties_to_monitor'],
        'configurable_parameters': probe['configurable_parameters']
      } for probe in monitoring_request.current_probes
    ],
    'data_engine': {
      'name': current_app.config['DATA_ENGINE_NAME'],
      'hosts': current_app.config['DATA_ENGINE_HOSTS'],
      'credentials': current_app.config['DATA_ENGINE_CREDENTIALS'],
      'source': current_app.config['DATA_ENGINE_SOURCE'],
      'config_location': current_app.config['DATA_ENGINE_CONFIG_LOCATION'],
      'needs_reconfiguration': current_app.config['DATA_ENGINE_NEEDS_RECONF']
    },
  }
  if monitoring_request.monitoring_units is not None:
    kwargs['monitoring_units'] = [
      {
        'id': mu['id'],
        'name': mu['name'],
        'probe_ids': mu['probe_ids'],
      } for mu in monitoring_request.monitoring_units
    ]

  get_monitoring_system_status_request = protobuf_messages.GetMonitoringSystemStatusRequest(**kwargs)

  with grpc.insecure_channel(current_app.config['MONITORING_ACTUATION_BRIDGE_URL']) as channel:
    stub = protobuf_grpc.MonitoringActuationBridgeStub(channel)
    get_monitoring_system_status_future = stub.GetMonitoringSystemStatus.future(get_monitoring_system_status_request)
    get_monitoring_system_status_result = get_monitoring_system_status_future.result()

    if get_monitoring_system_status_result.global_status == protobuf_messages.ERROR:
      monitoring_request.status = 'Error'
    elif get_monitoring_system_status_result.global_status == protobuf_messages.NEEDS_RECONFIGURATION:
      monitoring_request.status = 'Needs reconfiguration'
    elif get_monitoring_system_status_result.global_status == protobuf_messages.NEEDS_REDEPLOYMENT:
      monitoring_request.status = 'Needs redeployment'
    elif get_monitoring_system_status_result.global_status == protobuf_messages.DEPLOYED:
      monitoring_request.status = 'Deployed'
      monitoring_request.current_probes = monitoring_request.selected_probes
      monitoring_request.current_goals = monitoring_request.selected_goals

    monitoring_repository.save(monitoring_request)

@rq.exception_handler
def handle_failed_monitoring_request(job, *exec_info):
  if isinstance(job.args[0], MonitoringRequest):
    monitoring_request_id = str(job.args[0]._id)
  else:
    monitoring_request_id = job.args[0]
  monitoring_request = monitoring_repository.find(monitoring_request_id)
  monitoring_request.status = 'Error'
  monitoring_repository.save(monitoring_request)
  current_app.logger.error('Monitoring request %s failed at %s', monitoring_request_id, job.func_name)
