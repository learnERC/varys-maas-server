# -*- coding: utf-8 -*-

def test_show_tree(client):
  response = client.get('/api/v1/chmm')
  assert response.status_code == 200
  assert response.headers['Content-Type'] == 'application/json; charset=utf-8'
  json_data = response.get_json()
  assert json_data['name'] == 'monitoring_goals'
