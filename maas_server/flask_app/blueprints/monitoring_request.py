# -*- coding: utf-8 -*-

import json
from datetime import datetime
from flask import Blueprint, jsonify, request, current_app, make_response
import flask_app.services.monitoring_system as monitoring_system_service
from core.domain.monitoring import MonitoringRequest

monitoring_requests_api = Blueprint('monitoring_requests', __name__, url_prefix='/api/v1/monitoring-requests')

@monitoring_requests_api.route('/', methods=['POST'], strict_slashes=False)
def create():
  """Create a monitoring request given a monitoring definition as JSON payload."""
  monitoring_definition = request.json
  is_valid = validate_monitoring_definition(monitoring_definition)
  if True == is_valid:
    target = monitoring_definition['monitoring_request']['target']
    selected_goals = monitoring_definition['monitoring_request']['goals']
    monitoring_request = monitoring_system_service.create(target, selected_goals)
    return jsonify(status='success', message='Your monitoring request has been taken over!', monitoring_request=monitoring_request)
  return jsonify(status='error', message='The monitoring definition is invalid.')

@monitoring_requests_api.route('/<string:monitoring_request_id>', methods=['PATCH', 'PUT'], strict_slashes=False)
def update(monitoring_request_id):
  """Update a monitoring request given a monitoring definition as JSON payload."""
  monitoring_definition = request.json
  is_valid = validate_monitoring_definition(monitoring_definition)
  if True == is_valid:
    try:
      selected_goals = monitoring_definition['monitoring_request']['goals']
      monitoring_request = monitoring_system_service.update(monitoring_request_id, selected_goals)
      return jsonify(status='success', message='Your monitoring request update have been taken over!', monitoring_request=monitoring_request)
    except MonitoringRequest.DoesNotExist:
      return make_response(jsonify(status='error', message='Monitoring request not found.'), 404)
  return jsonify(error={'The monitoring definition is invalid.'})

@monitoring_requests_api.route('/<string:monitoring_request_id>', methods=['DELETE'], strict_slashes=False)
def delete(monitoring_request_id):
  """Delete a monitoring request."""
  try:
    monitoring_system_service.delete(monitoring_request_id)
    return jsonify(status='success', message='Your monitoring request has been deleted!')
  except MonitoringRequest.DoesNotExist:
    return make_response(jsonify(status='error', message='Monitoring request not found.'), 404)

@monitoring_requests_api.route('/', methods=['GET'], strict_slashes=False)
def get_all():
  """Get all currently available monitoring requests."""
  monitoring_requests = monitoring_system_service.get_all()
  return jsonify(list(monitoring_requests))

@monitoring_requests_api.route('/<string:monitoring_request_id>', methods=['GET'], strict_slashes=False)
def get(monitoring_request_id):
  """Get a monitoring request."""
  monitoring_request = monitoring_system_service.get(monitoring_request_id)
  return jsonify(monitoring_request)

def validate_monitoring_definition(monitoring_definition):
  """Validate a monitoring definition."""
  return True
